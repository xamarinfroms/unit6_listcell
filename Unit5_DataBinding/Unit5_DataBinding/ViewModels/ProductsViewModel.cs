﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Unit5.Core;
using Xamarin.Forms;
using XamarinUniversity.Interfaces;

namespace Unit5_DataBinding
{
    public class ProductsViewModel : INotifyPropertyChanged
    {
        public INavigationService NavigationService { get; set; }
        public ProductsViewModel()
        {
            ObservableCollection<ProductViewModel> items = new ObservableCollection<ProductViewModel>();
            foreach (var item in ProductFactory.Products)
                items.Add(new ProductViewModel(item));
            this.Products = items;


            this.OnUpdateData = new Command( () =>
              {

                  this.Products[0].Name = "超級飲水機二代";

              });
            this.OnGoDetail = new Command<Product>(this.GoDetail);


            this.OnRefresh = new Command(async () =>
              {
                  await Task.Delay(2000);
                  
                 //this.Products.Insert(0, new Product() { Name = "神秘的商品" });
                  this.IsRefreshing = false;
              });
        }

        public ICommand OnRefresh { get; private set; }

        private async void GoDetail(Product item)
        {
            await this.NavigationService.NavigateAsync(AppPage.ProductDetail, new ProductDetailViewModel(item));
        }

        public IList<ProductViewModel> Products { get; private set; }
        public Product SelectedProduct
        {
            get { return _selectedProduct; }
              set{ this._selectedProduct = value;this.PropertyChanged?.DynamicInvoke(this, new PropertyChangedEventArgs(nameof(this.SelectedProduct))); }
        }

        
 

        public bool IsRefreshing
        {
            get { return this._isRefreshing; }
            set
            {
                this._isRefreshing = value;
                this.PropertyChanged?.DynamicInvoke(this, new PropertyChangedEventArgs(nameof(this.IsRefreshing)));
            }
        }

    

        public ICommand OnUpdateData { get; private set; }

        public ICommand OnGoDetail { get; private set; }

        private Product _selectedProduct;
        private bool _isRefreshing;
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
